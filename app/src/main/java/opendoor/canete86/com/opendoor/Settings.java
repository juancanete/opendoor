package opendoor.canete86.com.opendoor;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;


public class Settings extends ActionBarActivity {

    private static final Integer REQUEST_ENABLE_BT = 1;
    private static final Integer ACCEPT_BUTTON_PUSHED = -1;

    public final String TAG = "Main";
    protected StringBuilder message = new StringBuilder();

    private ArrayList<BluetoothDevice> mDeviceList;

    Context context;

    //LIST OF ARRAY STRINGS WHICH WILL SERVE AS LIST ITEMS
    private ArrayList<String> listItemsBonded = new ArrayList<>();
    private ArrayList<String> listItems = new ArrayList<>();
    private ArrayList<BluetoothDevice> listDevices = new ArrayList<>();

    //DEFINING A STRING ADAPTER WHICH WILL HANDLE THE DATA OF THE LISTVIEW
    private ArrayAdapter<String> adapter;

    //View Elements
    private ListView listView;

    //Bluetooth
    Bluetooth bt;
    BluetoothAdapter mBluetooth = BluetoothAdapter.getDefaultAdapter();

    private boolean bIsConnected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        context = getApplicationContext();

        //Init primitive variables
        bIsConnected = false;

        //Initialize listView
        listView = (ListView) findViewById(R.id.lstDevices);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                connectService(listItems.get(position));
            }
        });

        adapter = new ArrayAdapter<String>(this, R.layout.simple_list, listItems);

        listView.setAdapter(adapter);

        //Check if bluetooth exist
        if(mBluetooth == null) {
            //Bluetooth doesn't exists
            showToast("I am sorry! You must have Bluetooth to work with this application.");
        } else {
            if (!mBluetooth.isEnabled()) {
                Intent enableBIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBIntent,REQUEST_ENABLE_BT);
            }else {
                initializeBluetooth();
            }
        }
    }
    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if(id == R.id.action_main_view){
            this.showMainView2();
        }else if (id == R.id.action_update) {
            connectService("HC-06");
        }

        return super.onOptionsItemSelected(item);
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void initializeBluetooth() {
        bt = new Bluetooth(this, mHandler);
        //Set<BluetoothDevice> pairedDevices = mBluetooth.getBondedDevices();

        //gotIt = getHC06Device(pairedDevices);

        /*if(gotIt) {
            status.setText("Paired");
            connectService("HC-06");
        }else {
            status.setText("Pairing...");
            pairDevice();
        }*/
    }

    public boolean getHC06Device(Set<BluetoothDevice> pairedDevices) {
        Iterator it = pairedDevices.iterator();
        BluetoothDevice device = null;
        boolean isHC06 = false;

        while(it.hasNext()) {
            device = (BluetoothDevice) it.next();

            if (device.getName().equalsIgnoreCase("hc-06")) {
                isHC06 = true;
                break;
            }
        }

        return isHC06;
    }

    /**********************Events************************************/
    /**
     *
     * @param view
     */
    /*public void changeSwitchValue(View view) {
        Switch swtState = (Switch)findViewById(R.id.swtOpenClose);
        message.delete(0, message.length());

        System.out.println(message + " length: " + message.length());

        if (swtState.isChecked()) {
            bt.sendMessage("1");
        }else {
            bt.sendMessage("0");
        }
    }*/

    public void lookForDevices(View view) {
        listItems.removeAll(listItems);
        mBluetooth.startDiscovery();

        // Register the BroadcastReceiver
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter); // Don't forget to unregister during onDestroy
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                // Add the name and address to an array adapter to show in a ListView
                listItems.add(device.getName());

                if(listItems.size() > 0) {
                    if(device.getName().equalsIgnoreCase("hc-06")) {
                        try {
                            //it tries to pair the bluetooth device
                            Method method = device.getClass().getMethod("createBond", (Class[]) null);
                            method.invoke(device, (Object[]) null);
                            //status.setText("Paired");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else {
                        //status.setText("Device hasn't been found!");
                    }
                }

                adapter.notifyDataSetChanged();
            }

            if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                final int state        = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
                final int prevState    = intent.getIntExtra(BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE, BluetoothDevice.ERROR);

                if (state == BluetoothDevice.BOND_BONDED && prevState == BluetoothDevice.BOND_BONDING) {
                    showToast("Paired");
                } else if (state == BluetoothDevice.BOND_NONE && prevState == BluetoothDevice.BOND_BONDED){
                    showToast("Unpaired");
                }
            }
        }
    };

    public void connectService(String deviceName){
        try {
            if (!bIsConnected) {

                if (mBluetooth.isEnabled()) {
                    bt.start();
                    bt.connectDevice(deviceName);
                    Log.d(TAG, "Btservice started - listening");

                    while(true) {
                        System.out.println("Connecting?????????? " + bt.getState() + " Bluet: " + Bluetooth.STATE_CONNECTED);
                        if(bt.getState() != Bluetooth.STATE_CONNECTED) {
                            bIsConnected = true;

                            break;
                        }
                        Thread.sleep(500);
                    }
                } else {
                    Log.w(TAG, "Btservice started - bluetooth is not enabled");
                }
            }
        } catch(Exception e){
            Log.e(TAG, "Unable to start bt ", e);
            //status.setText("Unable to connect " +e);
            bIsConnected = false;
        }
    }

    public void pairDevice() {
        boolean existDevice = false;

        listDevices.removeAll(listDevices);
        mBluetooth.startDiscovery();

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter); // Don't forget to unregister during onDestroy
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == ACCEPT_BUTTON_PUSHED) {
            initializeBluetooth();
        }
    }

    //@Override
    //public void onBackPressed() {
        //Log.d("CDA", "onBackPressed Called");
        //moveTaskToBack(true);
        /*Intent intent = new Intent();
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);

        startActivity(intent);*/
    //}

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Bluetooth.MESSAGE_STATE_CHANGE:
                    Log.d(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    break;
                case Bluetooth.MESSAGE_WRITE:
                    Log.d(TAG, "MESSAGE_WRITE ");
                    break;
                case Bluetooth.MESSAGE_READ:

                    Log.d(TAG, "MESSAGE_READ " + msg.obj);

                    message.append(msg.obj);

                    /*String tmpMessage = message.toString();
                    response.setText(tmpMessage);*/

                    break;
                case Bluetooth.MESSAGE_DEVICE_NAME:
                    Log.d(TAG, "MESSAGE_DEVICE_NAME "+msg);
                    break;
                case Bluetooth.MESSAGE_TOAST:
                    Log.d(TAG, "MESSAGE_TOAST "+msg);
                    break;
            }
        }
    };

    /***********************List View*********************************/
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        PlanetHolder holder = new PlanetHolder();

        // First let's verify the convertView is not null
        if (convertView == null) {
            // This a new view we inflate the new layout
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.simple_list, null);
            // Now we can fill the layout with the right values
            TextView tv = (TextView) v.findViewById(R.id.txtDeviceName);

            holder.deviceName = tv;

            v.setTag(holder);

            v.setBackgroundResource(R.drawable.rounded_corners);
        }
        else
            holder = (PlanetHolder) v.getTag();

        //Planet p = planetList.get(position);
        //holder.planetNameView.setText(p.getName());

        return v;
    }

    private static class PlanetHolder {
        public TextView deviceName;
    }
}

